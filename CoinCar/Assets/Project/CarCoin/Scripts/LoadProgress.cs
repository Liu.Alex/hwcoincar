using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoadProgress : MonoBehaviour
{
    private static LoadProgress _instance = null;
    public static LoadProgress Instance() { return _instance; }


    private float _progress = 0.0f;
    public Text tmp;

    private void Awake()
    {
        _instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {

    }

    public void EableProgress()
    {
        _progress = 0.0f;
        this.gameObject.SetActive(true);
        UpdateProgress(0);
    }

    public void UpdateProgress(float p)
    {
        _progress = p;
        if (_progress < 0)
        {
            _progress = 0;
        }
        else if (_progress > 1.0f)
        {
            _progress = 1.0f;
        }
        int iPercent = Mathf.FloorToInt(_progress * 100.0f);
        tmp.text = "Loading..." + iPercent.ToString() + "%";
    }

    public void EndProgress()
    {
        _progress = 0.0f;
        this.gameObject.SetActive(false);
    }

    void Update()
    {
        //  _progress += Time.deltaTime*0.01f;
        //  UpdateProgress(_progress);
        //  Debug.Log("_progress " + Time.deltaTime);
    }
}
