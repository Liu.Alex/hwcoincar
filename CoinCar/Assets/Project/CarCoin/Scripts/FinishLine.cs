using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour
{
    public Object winEffect;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Car")
        {
            Debug.Log("Win");
            Instantiate(winEffect, transform.position, transform.rotation);
            Instantiate(winEffect, transform.position, transform.rotation);
            Instantiate(winEffect, transform.position, transform.rotation);
            gameObject.SetActive(false);
        }
    }



}
