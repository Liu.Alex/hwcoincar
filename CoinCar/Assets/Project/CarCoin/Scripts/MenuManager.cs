using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    private static MenuManager _instance = null;
    public static MenuManager Instance() { return _instance; }


    public GameObject loadingObject;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            GameObject go = GameObject.Instantiate(loadingObject);
            go.SetActive(false);
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    IEnumerator customLoadData(string scName)
    {
        if (scName == "GameScene" || scName == "Menu")
        {
            float progress = 0.5f;
            for (var i = 0; i < 10; i++)
            {
                progress += i * 0.01f;
                GameObject go = new GameObject();
                LoadProgress.Instance().UpdateProgress(progress);
                yield return 0;
            }
            // setup player
            progress = 0.6f;
            LoadProgress.Instance().UpdateProgress(progress);
            yield return new WaitForSeconds(1.0f);
            // spawn enemy
            progress = 0.7f;
            LoadProgress.Instance().UpdateProgress(progress);
            yield return new WaitForSeconds(1.0f);

            //perform
            #region
            progress = 0.75f;
            LoadProgress.Instance().UpdateProgress(progress);
            yield return new WaitForSeconds(0.15f);
            progress = 0.77f;
            LoadProgress.Instance().UpdateProgress(progress);
            yield return new WaitForSeconds(0.15f);
            progress = 0.79f;
            LoadProgress.Instance().UpdateProgress(progress);
            yield return new WaitForSeconds(0.15f);
            progress = 0.81f;
            LoadProgress.Instance().UpdateProgress(progress);
            yield return new WaitForSeconds(0.15f);
            progress = 0.84f;
            LoadProgress.Instance().UpdateProgress(progress);
            yield return new WaitForSeconds(0.15f);
            progress = 0.87f;
            LoadProgress.Instance().UpdateProgress(progress);
            yield return new WaitForSeconds(0.15f);
            progress = 0.91f;
            LoadProgress.Instance().UpdateProgress(progress);
            yield return new WaitForSeconds(0.15f);
            progress = 0.95f;
            LoadProgress.Instance().UpdateProgress(progress);
            yield return new WaitForSeconds(1f);
            #endregion

            // setup camera 
            progress = 0.99f;
            LoadProgress.Instance().UpdateProgress(progress);
            yield return new WaitForSeconds(3.0f);

            // setup camera 
            progress = 1.0f;
            LoadProgress.Instance().UpdateProgress(progress);
            yield return new WaitForSeconds(0.5f);

            // Disable Loading
            LoadProgress.Instance().EndProgress();
        }
        yield break;
    }

    void FinishLoadScene(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
    {
        //Debug.Log("FinishLoadScene " + scene.name);
        StartCoroutine(customLoadData(scene.name));

    }

    // Start is called before the first frame update
    void Start()
    {
        SceneLoad sc = SceneLoad.Instance();
        if (sc == null)
        {
            sc = new SceneLoad();
            sc.Init();
        }
        sc.RegisterCallback(FinishLoadScene);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (SceneManager.GetActiveScene().name == "Menu")
            {
                StartCoroutine(SceneLoad.Instance().ChangeSceneAsync("GameScene"));
                // SceneManager.LoadScene("fps", LoadSceneMode.Single);
            }

            //else
            //{
            //    StartCoroutine(SceneLoad.Instance().ChangeSceneAsync("Menu"));
            //    //SceneManager.LoadScene("menu", LoadSceneMode.Single);
            //}
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
