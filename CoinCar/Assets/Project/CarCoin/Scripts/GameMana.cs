using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMana : MonoBehaviour
{
    private static GameMana _instance = null;
    public static GameMana Instance() { return _instance; }
    public GameObject playerCar;
    public Transform tPlayerCar;
    public GameObject gameCoin;
    private GameObject[] _coins;
    public Transform cameraHolder;
    public GameObject losePanel;
    public GameObject winPanel;
    public GameObject optionPanel;
    public GameObject finishLine;
    public float cameraRotate = 0;
    //public GameObject bgMusic;
    public bool gameRun { get; set; }
    public Transform[] coinPosion;



    private void Awake()
    {
        //DeleteMusic();
        _instance = this;
        
    }
    void Start()
    {
        //GenerateMusic();
        GeneratePlayer();
        GenerateCoin();
        GameStart();

    }

    // Update is called once per frame
    void Update()
    {
        WinPanel();
        LosePanel();
        OptionPanel();
    }

    void LateUpdate()
    {
        SetCarCamera();
        GameWin();
        GameLose();
        Option();
    }



    public void GeneratePlayer()
    {
        playerCar.transform.position = tPlayerCar.position;
        //Instantiate(playerCar, tPlayerCar.position, tPlayerCar.rotation, tPlayerCar);
    }
    public void RemoveEnemy(GameObject go)
    {
        for (int i = 0; i < _coins.Length; i++)
        {
            if (_coins[i] == go)
            {
                _coins[i] = null;
            }
        }
    }
    public void GenerateCoin()
    {
        Instantiate(gameCoin, coinPosion[0].transform);
        Instantiate(gameCoin, coinPosion[1].transform);
        Instantiate(gameCoin, coinPosion[2].transform);
        Instantiate(gameCoin, coinPosion[3].transform);
        Instantiate(gameCoin, coinPosion[4].transform);
        Instantiate(gameCoin, coinPosion[5].transform);
        Instantiate(gameCoin, coinPosion[6].transform);


    }

    //public void GenerateMusic()
    //{
    //    //if (GameObject.Find("music") != null)
    //    //{
    //    //    //Debug.Log("music�I");
    //    //    return;
    //    //}
    //    //else
    //    //{
    //    //    Instantiate(bgMusic);
    //    //    //Debug.Log("music���b");
    //    //}

    //}

    /*private void GenerateEnemies(int num)
    {
        _coins = new GameObject[num];
        for (int i = 0; i < num; i++)
        {
            GameObject go = GameObject.Instantiate(gameCoin) as GameObject;
            Vector3 vdir = new Vector3(UnityEngine.Random.Range(-50.0f,50.0f),1.5f, UnityEngine.Random.Range(-50.0f, 50.0f));
            go.transform.position = vdir * UnityEngine.Random.Range(.8f, .8f);
            _coins[i] = go;
        }
    }*/
    public void GameStart()
    {
        gameRun = true;
        return;
    }
    public void SetCarCamera()
    {
        if (gameRun == true)
        {
            cameraHolder.position = playerCar.transform.position;
        }
    }

    public void WinPanel()
    {
        if (winPanel.active)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(0);

                //StartCoroutine(SceneLoad.Instance().ChangeSceneAsync("GameScene"));
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(1);
                //StartCoroutine(SceneLoad.Instance().ChangeSceneAsync("Menu"));
            }
        }
        
    }

    public void LosePanel()
    {
        if (losePanel.active == true)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(0);

                //StartCoroutine(SceneLoad.Instance().ChangeSceneAsync("GameScene"));
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(1);
                //StartCoroutine(SceneLoad.Instance().ChangeSceneAsync("Menu"));
            }
        }

    }
    public void OptionPanel()
    {
        if (optionPanel.active == true)
        {
            if (Input.GetKeyDown(KeyCode.F4))
            {
                Application.Quit();
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(0);
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(1);
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
                optionPanel.SetActive(false);
                return;
            }

        }

    }

    public void GameLose()
    {
        if (playerCar.active == false)
        {
            //Debug.Log("lose");
            losePanel.SetActive(true);
        }
    }

    public void GameWin()
    {
        if (finishLine.active == false)
        {
            winPanel.SetActive(true);
        }
    }
    
    public void Option()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            optionPanel.SetActive(true);
        }

    }
    




}
