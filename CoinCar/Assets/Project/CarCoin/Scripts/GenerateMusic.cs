using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GenerateMusic : MonoBehaviour
{
    private static GenerateMusic instance;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;

        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {

        if (SceneManager.GetActiveScene().name == "Menu")
        {
            Debug.Log("MenuScene");
            Destroy(gameObject);
            return;
        }
    }
}