using UnityEngine;


    public class Obstacle : MonoBehaviour
    {
        [Tooltip("The effect that is created at the location of this object when it is hit")]
        public Transform hitWallEffect;

        [Tooltip("Should this obstacle be removed when hit by a car?")]
        public bool removeOnHit = false;

        [Tooltip("A random rotation given to the object only on the Y axis")]
        public float randomRotation = 360;

        void Start()
        {
            // Resets the color of an obstacle periodically
            //InvokeRepeating("ResetColor", 0, 0.5f);
        }

        void OnTriggerStay(Collider other)
        {
            // If there is a hit effect, create it
            if (hitWallEffect) Instantiate(hitWallEffect, transform.position, transform.rotation);
            // Remove the object from the game
            if (removeOnHit == true) Destroy(gameObject);
        }

        public void ResetColor()
        {
            GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.black);
        }
    }
